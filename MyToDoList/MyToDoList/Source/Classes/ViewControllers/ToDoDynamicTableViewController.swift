//
//  ToDoDynamicTableViewController.swift
//  MyToDoList
//
//  Created by Yura Ostrovskii on 8/31/17.
//  Copyright © 2017 YuraOstrovskii. All rights reserved.
//

import UIKit
import MessageUI

class  ToDoDynamicTableViewController: UIViewController, MFMailComposeViewControllerDelegate {

    //MARK: - Properties
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var saveButton: UIBarButtonItem!

    @IBAction func eMailButtonTapped(_ sender: UIButton) {

        let mailVC = MFMailComposeViewController()
        mailVC.mailComposeDelegate = self
        mailVC.setToRecipients([])
        mailVC.setSubject("Subject for email")
        mailVC.setMessageBody("Email message string", isHTML: false)

        if MFMailComposeViewController.canSendMail() {
            present(mailVC, animated: true, completion: nil)
        }
    }

    func mailComposeController(controller:MFMailComposeViewController, didFinishWithResult result:MFMailComposeResult, error:Error?) {
        switch result {
        case .cancelled:
            print("Mail cancelled")
        case .saved:
            print("Mail saved")
        case .sent:
            print("Mail sent")
        case .failed:
            print("Mail sent failure: \(error?.localizedDescription)")
        }
        self.dismiss(animated: true, completion: nil)
    }

    var isPickerHidden = true
    var todo: ToDo?
    var titleText: String?
    var isCompleteSelected: Bool = false
    var notesText: String = ""
    var dueDatePicker: Date?

    //MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        registerCustomCells()
        updateUI()
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)

        guard segue.identifier == "SaveUnwind", let title = titleText, let dueDate = dueDatePicker else { return }

        todo = ToDo(title: title, isComplete: isCompleteSelected, dueDate: dueDate, notes: notesText)
    }

    func registerCustomCells() {
        tableView.register(UINib(nibName: "\(BasicInfoCell.self)", bundle: nil), forCellReuseIdentifier: "\(BasicInfoCell.self)")
        tableView.register(UINib(nibName: "\(NotesCell.self)", bundle: nil), forCellReuseIdentifier: "\(NotesCell.self)")
        tableView.register(UINib(nibName: "\(DatePickerCellTableViewCell.self)", bundle: nil), forCellReuseIdentifier: "\(DatePickerCellTableViewCell.self)")
    }
}

//MARK: - UITableViewDelegate
extension ToDoDynamicTableViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        switch indexPath.section {
        case ToDoCellType.dueDate.rawValue:
            isPickerHidden = !isPickerHidden
            tableView.beginUpdates()
            tableView.endUpdates()
        default:
            break
        }
    }
}

//MARK: - UITableViewDataSource
extension ToDoDynamicTableViewController: UITableViewDataSource {
    public func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }

    public func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }

    public func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        switch indexPath.section {
        case ToDoCellType.basicInfo.rawValue:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "\(BasicInfoCell.self)") as? BasicInfoCell else {
                fatalError("Can't unwrap to BasicInfoCell")
            }
            cell.isCopmleteButton.isSelected = isCompleteSelected
            cell.titleTextField.text = titleText

            cell.tapAction = { [weak self] (cell) in
                self?.isCompleteSelected = cell.isCopmleteButton.isSelected
            }

            cell.textFieldAction = { [weak self] (cell) in
                self?.titleText = cell.titleTextField.text
                self?.updateSaveButtonState()
            }

            return cell
        case ToDoCellType.dueDate.rawValue:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "\(DatePickerCellTableViewCell.self)") as? DatePickerCellTableViewCell else {
                fatalError("Can't unwrap to DatePickerCellTableViewCell")
            }

            guard let dueDatePicker = dueDatePicker  else {
                fatalError("Can't unwrap dueDatePicker")
            }
            cell.dueDatePickerView.date = dueDatePicker
            cell.updateDueDateLabel(date: dueDatePicker)

            cell.didSelectDatepicker = { [weak self] (cell) in
                self?.dueDatePicker = cell.dueDatePickerView.date
            }

            return cell
        default:
            guard let cell = tableView.dequeueReusableCell(withIdentifier: "\(NotesCell.self)") as? NotesCell else {
                fatalError("Can't unwrap to NotesCell")
            }
            cell.notesTextField.text = notesText

            cell.didChangeNotesText = {[weak self] (cell) in
                self?.notesText = cell.notesTextField.text
            }

            return cell
        }
    }

    public func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        switch section {
        case ToDoCellType.basicInfo.rawValue:
            return ToDoCellType.basicInfo.title
        case ToDoCellType.dueDate.rawValue:
            return ToDoCellType.dueDate.title
        default:
            return ToDoCellType.notes.title
        }
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let normalCellHeight = CGFloat(44)
        let largeCellHeight = CGFloat(200)

        switch indexPath.section {
        case ToDoCellType.dueDate.rawValue:
            return isPickerHidden ? normalCellHeight : largeCellHeight
        case ToDoCellType.notes.rawValue:
            return largeCellHeight
        default:
            return normalCellHeight
        }
    }
}

//MARK: - Update methods
extension ToDoDynamicTableViewController {
    func updateUI() {
        if let todo = todo {
            navigationItem.title = "To-Do"
            titleText = todo.title
            isCompleteSelected = todo.isComplete
            dueDatePicker = todo.dueDate
            notesText = todo.notes ?? ""
        } else {
            dueDatePicker = Date().addingTimeInterval(24*60*60)
        }
        updateSaveButtonState()
    }

    func updateSaveButtonState() {
        let text = titleText ?? ""
        saveButton.isEnabled = !text.isEmpty
    }
}
