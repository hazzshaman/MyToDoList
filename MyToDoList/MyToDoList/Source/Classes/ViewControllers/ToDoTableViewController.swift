//
//  ToDoTableViewController.swift
//  MyToDoList
//
//  Created by Yura Ostrovskii on 8/31/17.
//  Copyright © 2017 YuraOstrovskii. All rights reserved.
//

import UIKit

class ToDoTableViewController: UITableViewController {
    //MARK: - Properties
    var todos = [ToDo]()

    //MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        updateUI()
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard segue.identifier == "ShowDetails" else { return }

        guard let todoViewController = segue.destination as? ToDoDynamicTableViewController,
            let indexPath = tableView.indexPathForSelectedRow else {
                fatalError("Segue destination can't convert to ToDoViewController")
        }

        let selectedToDo = todos[indexPath.row]
        todoViewController.todo = selectedToDo
    }

    //MARK: - UITableViewDatasource
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return todos.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "\(ToDoCell.self)") as? ToDoCell else {
            fatalError("Could not dequeue a cell")
        }

        let todo = todos[indexPath.row]
        cell.titlelabel?.text = todo.title
        cell.isCopleteButton.isSelected = todo.isComplete
        cell.delegate = self

        return cell
    }

    //MARK: - UITableViewDelegate
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }

    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        guard editingStyle == .delete else { return }
        todos.remove(at: indexPath.row)
        tableView.deleteRows(at: [indexPath], with: .fade)
        ToDo.saveToDos(todos)
    }

    //MARK: - Save Unwind Segue
    @IBAction func unwindToDoList(segue: UIStoryboardSegue) {
        guard segue.identifier == "SaveUnwind" else { return }

        guard let sourceViewController = segue.source as? ToDoDynamicTableViewController else {
            fatalError("Can't convert to ToDoDynamicTableViewController")
        }

        if let todo = sourceViewController.todo {

            if let selectedIndexPath = tableView.indexPathForSelectedRow {
                todos[selectedIndexPath.row] = todo
                tableView.reloadRows(at: [selectedIndexPath], with: .none)
            } else {
                let newIndexPath = IndexPath(row: todos.count, section: 0)
                todos.append(todo)
                tableView.insertRows(at: [newIndexPath], with: .automatic)
            }
        }
        ToDo.saveToDos(todos)
    }
}

//MARK: - Update UI and load data
extension ToDoTableViewController {
    func updateUI() {
        navigationItem.leftBarButtonItem = editButtonItem

        guard let savedToDos = ToDo.loadToDos() else {
            todos = ToDo.loadSimpleToDos()
            return
        }
        todos = savedToDos
    }
}

//MARK: - ToDoCellDelegate
extension ToDoTableViewController: ToDoCellDelegate {
    func checkmarkTapped(sender: ToDoCell) {
        guard let indexPath = tableView.indexPath(for: sender) else { return }

        let todo = todos[indexPath.row]
        todo.isComplete = !todo.isComplete
        tableView.reloadRows(at: [indexPath], with: .automatic)
        ToDo.saveToDos(todos)
    }
}
