//
//  ToDoCellType.swift
//  MyToDoList
//
//  Created by Yura Ostrovskii on 8/31/17.
//  Copyright © 2017 YuraOstrovskii. All rights reserved.
//

import Foundation

enum ToDoCellType: Int {

    case basicInfo
    case dueDate
    case notes

    var title: String {
        switch self {
        case .basicInfo:
            return "Basic Info"
        case .dueDate:
            return "Date"
        case .notes:
            return "Notes"
        }
    }
}
