//
//  BasicInfoCell.swift
//  MyToDoList
//
//  Created by Yura Ostrovskii on 8/31/17.
//  Copyright © 2017 YuraOstrovskii. All rights reserved.
//

import UIKit

class BasicInfoCell: UITableViewCell {
    
//MARK: - Properties
    var tapAction: ((BasicInfoCell) -> ())?
    var textFieldAction: ((BasicInfoCell) -> ())?
    
    @IBOutlet weak var isCopmleteButton: UIButton!
    @IBOutlet weak var titleTextField: UITextField!
}

//MARK: - Actions
extension BasicInfoCell {
    @IBAction func isCompleteButtonTapped(_ sender: UIButton) {
        isCopmleteButton.isSelected = !isCopmleteButton.isSelected
        tapAction?(self)
    }
    
    @IBAction func valueChanged(_ sender: UITextField) {
        textFieldAction?(self)
    }
}
