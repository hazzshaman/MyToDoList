//
//  DatePickerCellTableViewCell.swift
//  MyToDoList
//
//  Created by Yura Ostrovskii on 8/31/17.
//  Copyright © 2017 YuraOstrovskii. All rights reserved.
//
import UIKit

class DatePickerCellTableViewCell: UITableViewCell {
    
//MARK: - Properties
    @IBOutlet weak var dueDateLabel: UILabel!
    @IBOutlet weak var dueDatePickerView: UIDatePicker!
    
    var didSelectDatepicker: ((DatePickerCellTableViewCell) -> ())?
    
//MARK: - Update Label Method
    func updateDueDateLabel(date: Date) {
        dueDateLabel.text = ToDo.dueDateFormatter.string(from: date)
    }
}

//MARK: - Actions
extension DatePickerCellTableViewCell {
    @IBAction func selectedDatePicker(_ sender: UIDatePicker) {
        updateDueDateLabel(date: sender.date)
        didSelectDatepicker?(self)
    }
}
