//
//  ToDoCell.swift
//  MyToDoList
//
//  Created by Yura Ostrovskii on 8/31/17.
//  Copyright © 2017 YuraOstrovskii. All rights reserved.
//

import UIKit

//MARK: - protocol ToDoDelegate
@objc protocol ToDoCellDelegate: class {
   func checkmarkTapped(sender: ToDoCell)
}

//MARK: - class ToDoCell
class ToDoCell: UITableViewCell {
    @IBOutlet weak var isCopleteButton: UIButton!
    @IBOutlet weak var titlelabel: UILabel!
    
    weak var delegate: ToDoCellDelegate?
}

//MARK: - Actions
extension ToDoCell {
    @IBAction func completeButtonTapped(_ sender: UIButton) {
        delegate?.checkmarkTapped(sender: self)
    }
}
