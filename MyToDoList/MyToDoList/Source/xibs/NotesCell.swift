//
//  NotesCell.swift
//  MyToDoList
//
//  Created by Yura Ostrovskii on 8/31/17.
//  Copyright © 2017 YuraOstrovskii. All rights reserved.
//

import UIKit

class NotesCell: UITableViewCell {
    
    //MARK: - Properties
    @IBOutlet weak var notesTextField: UITextView!
    var didChangeNotesText: ((NotesCell) -> ())?
}
